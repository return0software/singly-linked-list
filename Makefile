CC = gcc
CFLAGS = -std=c11 -Wall -Wextra -pedantic -g

all:
	$(CC) $(CFLAGS) tests/test.c -o ssl
