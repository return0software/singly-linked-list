# **Templated** C Singly Linked List

## Building

We have 3 ways of building the test executable.

### Makefile

        make
        ./ssl

### Meson

        meson builddir
        ninja
        ./sll
