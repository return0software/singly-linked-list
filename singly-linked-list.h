#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

////////////////////////////////////////////////////////////////////////////////
// SLNode Implementation
////////////////////////////////////////////////////////////////////////////////

/*
 * SLNode definition
 */
typedef struct SLNode {
        void *elem;
        struct SLNode *next;
} SLNode;

/*
 * params: None
 * return: SLNode*
 * function: Create an empty SLNode
 */
SLNode* slnode_init(void) {
        return (SLNode*) malloc(sizeof(SLNode));
}

/*
 * params: void*
 * return: SLNode*
 * function: Create an SLNode from an elem
 */
SLNode* slnode_init_from_elem(void *elem) {
        SLNode* new_node = slnode_init();
        new_node->elem = elem;
        return new_node;
}

/*
 * params: *SLNode
 * return: SLNode*
 * function: Create an SLNode from an SLNode
 */
SLNode* slnode_init_from_slnode(SLNode *node) {
        SLNode* new_node = slnode_init();
        new_node->elem = node->elem;
        return new_node;
}

////////////////////////////////////////////////////////////////////////////////
// SList Implementation
////////////////////////////////////////////////////////////////////////////////

/*
 * SList definition
 */
typedef struct SList {
        SLNode *head;
        SLNode *tail;
        uint32_t length;
} SList;

/*
 * params: None
 * return: pointer to list
 * function: Create an empty SList
 */
SList* slist_init(void)
{
        return (SList*) malloc(sizeof(SList));
}

/*
 * params: SList*, void*
 * return: index of added element
 * function: Add element to the end of the list
 */
uint32_t slist_append(SList *list, void *elem)
{
        SLNode *n = slnode_init_from_elem(elem);

        // if list is empty
        if (list->head == NULL) {
                list->head = n;
                list->tail = n;
        } else {
                list->tail->next = n;
                list->tail = list->tail->next; // &?
        }

        return list->length++;
}

/*
 * params: SList*
 * return: pointer to list
 * function: Create a List based on another SList
 */
SList* slist_init_from_slist(SList *from_list)
{
        if (from_list == NULL) {
                return NULL;
        }

        // start initialization
        SList* to_list = slist_init();
        to_list->length = from_list->length;

        for (SLNode *from_curr = from_list->head; from_curr != from_list->tail; from_curr++) {
                slist_append(to_list, from_curr->elem);
        }

        return to_list;
}

/*
 * params: SList*, void*
 * return: index of added
 * function: Add element to the front of the list
 */
uint32_t slist_prepend(SList *list, void *elem)
{
        SLNode *n = slnode_init_from_elem(elem);
        n->next = list->head;
        list->head = n;

        list->length++;
        return 0;
}

/*
 * params: SList*, uint32_t
 * return: void*
 * function: Return the element located at index
 */
void* slist_get(SList *list, uint32_t index)
{
        if (index >= list->length) {
                return NULL;
        }

        SLNode *curr = list->head;
        for (uint32_t i = 0; i < index; i++) {
                curr = curr->next;
        }

        return curr->elem;
}

/*
 * params: SList*, void*, uint32_t
 * return: uint32_t
 * function: Set the element located at index
 */
uint32_t slist_set(SList *list, void *elem, uint32_t index)
{
        if (list->length >= index){
                return 1;
        }

        SLNode *temp = list->head;
        uint32_t i = 0;
        while (temp->next != NULL && index != i) {
                temp = temp->next;
                i++;
        }

        temp->elem = elem;
        return 0;
}

/*
 * params: SList*, void*, uint32_t
 * return: uint32_t
 * function: Insert element at the specified index
 */
uint32_t slist_insert_at(SList *list, void* elem, uint32_t index)
{
        if (index >= list->length) {
                return 1;
        }

        SLNode *curr = list->head;
        for (uint32_t i = 0; i < index; i++) {
                curr = curr->next;
        }

        SLNode *new_node = slnode_init_from_elem(elem);
        new_node->next = curr->next;
        curr->next = new_node;

        return 0;
}

/*
 * params: SList*
 * return: uint32_t
 * function: Set the element located at index
 */
uint32_t slist_remove_beginning(SList *list)
{
        if (list->head != NULL) {
                SLNode *n = list->head;
                if (n->next != NULL) {
                        list->head = n->next;
                } else {
                        list->head = NULL;
                }
                free(n);
        }
        return 0;
}

/*
 * params: SList*, void*, uint32_t
 * return: uint32_t
 * function: Set the element located at index
 */
uint32_t slist_remove_end(SList *list)
{
        if (list->head != NULL) {;
                if (list->head->next == NULL) {
                        return slist_remove_beginning(list);
                } else {
                        SLNode *n = list->head;
                        while (n->next->next != NULL) {
                                n = n->next;
                        }
                        n->next = NULL;
                        free(n->next->next);
                }
        }
        return 0;
}