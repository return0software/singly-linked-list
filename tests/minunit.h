// Minunit http://www.jera.com/techinfo/jtns/jtn002.html

#define mu_assert(message, test) do { if (!(test)) return message; } while (0)
#define mu_run_test(test) do { char *message = test(); TESTS_RUN++; \
                        if (message) return message; } while (0)
extern int TESTS_RUN;
