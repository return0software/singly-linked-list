#include <stdio.h>
#include <stdint.h>
#include "singly-linked-list.h"
#include "minunit.h"

int TESTS_RUN;

static char *test_append()
{
        SList* list = slist_init();

        int expected[] = {0, 1, 2};
        for (int i = 0; i < 3; i++) {
                slist_append(list, (void*) &expected[i]);
        }

        int same = 1;
        int compare[3];
        for (int i = 0; i < 3; i++) {
                compare[i] = *(int*) slist_get(list, i);
                if (compare[i] != expected[i]) {
                        same = 0;
                        break;
                }
        }

        mu_assert("Error: Append test is not working!", same == 1);
        return 0;
}

static char *all_tests()
{
        mu_run_test(test_append);
        return 0;
}

int main(__attribute__((unused)) int argc, __attribute__((unused)) const char *argv[])
{
        char *result = all_tests();
        if (result != 0) {
                printf("%s\n", result);
        } else {
                printf("ALL TESTS PASSES\n");
        }
        printf("Tests run: %d\n", TESTS_RUN);

        return result != 0;
}
